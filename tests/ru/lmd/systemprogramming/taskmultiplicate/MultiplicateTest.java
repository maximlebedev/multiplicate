package ru.lmd.systemprogramming.taskmultiplicate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultiplicateTest {

    @Test
    public void multiplyPositive() {
        int a = 1 + (int) (Math.random() * 100);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a * b, Multiplicate.multiply(a, b));
    }

    @Test
    public void multiplyNegative() {
        int a = -100 + (int) (Math.random() * 99);
        int b = -100 + (int) (Math.random() * 99);
        assertEquals(a * b, Multiplicate.multiply(a, b));
    }

    @Test
    public void multiplyPositiveOnNegative() {
        int a = 1 + (int) (Math.random() * 100);
        int b = -100 + (int) (Math.random() * 99);
        assertEquals(a * b, Multiplicate.multiply(a, b));
    }

    @Test
    public void multiplyNegativeOnPositive() {
        int a = -100 + (int) (Math.random() * 99);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a * b, Multiplicate.multiply(a, b));
    }

    @Test
    public void multiplyZeroByZero() {
        int a = 0;
        int b = 0;
        assertEquals(0, Multiplicate.multiply(a, b));
    }

    @Test
    public void multiplyNumByZero() {
        int a = 1 + (int) (Math.random() * 100);
        int b = 0;
        assertEquals(a * b, Multiplicate.multiply(a, b));
    }

    @Test
    public void multiplyZeroByNum() {
        int a = 0;
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a * b, Multiplicate.multiply(a, b));
    }
}